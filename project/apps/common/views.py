import random

from django.shortcuts import render



def home(request):
    """View function for home page of site."""
    rand_number = random.randint(1,1000)
    return render(request, 'home.html', {'random' :rand_number})
